import React, { Component } from 'react';
import logo from '../images/logo.png';
import SignUp from './signup'
import Login from './login';
import '../App.scss'
import { Link, Route, BrowserRouter as Router, Switch } from 'react-router-dom';
class Navbar extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    render() {
        return (
            <>
                <Router>
                    <div className="navbar">
                        <Link to="/">
                            <span id="brandname">
                                <img src={logo} alt="sixsprintslog" />
                            </span>
                        </Link>
                        <Link to="/"><span id="navbar1">Home</span></Link>
                        <Link to="/login"><span id="navbar3">Login</span></Link>
                        <Link to="/signup"><span id="navbar4">SignUp</span></Link>
                    </div>
                    <Switch>
                        <Route exact path="/login">
                            <Login />
                        </Route>
                        <Route exact path="/signup">
                            <SignUp />
                        </Route>
                        <Route exact path="/">
                            <App />
                        </Route>
                    </Switch>
                </Router>
            </>
        );
        function App() {
            return null;
        };
        




    }
}

export default Navbar;
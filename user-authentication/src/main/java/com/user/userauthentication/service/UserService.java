package com.user.userauthentication.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.user.userauthentication.repository.UserRepository;
import com.user.userauthentication.user.User;

@Service
public class UserService {
	
	@Autowired
	private UserRepository userRepository;
	
	public User save(User user) {
		return userRepository.save(user);
	}
	
	public List<User> findByUsernameAndPassword(String username,String password){
		return userRepository.findByUsernameAndPassword(username, password);
	}
	
}
